package be.kdg.pro1.m4;

public class ConcatenateStrings {
  public static void main(String[] args) {
    String me = "me";
    int number = 2 + 3;
    String result = me + " a ";
    result += number  + "!";
    System.out.println("Give " + result);
    System.out.println(3 + 7 + " hello world " + 3 + 7);

    String s1 = "abc";
    s1.concat("def");
    System.out.println(s1);
    String s2 = s1.concat("def");
    System.out.println(s2);

  }
}
