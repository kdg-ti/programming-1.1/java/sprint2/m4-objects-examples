package be.kdg.pro1.m4;

public class StringBuilderExamples {
  public static void main(String[] args) {
    StringBuilder spoon = new StringBuilder("spoon");
    StringBuilder fork = new StringBuilder();
    fork.append('f');
    fork.append('o');
    fork.append('r').append('k');
    System.out.println(spoon + " and " + fork);

    StringBuilder builder = new StringBuilder("builder");
    builder.reverse();
    System.out.println(builder);
  }
}
