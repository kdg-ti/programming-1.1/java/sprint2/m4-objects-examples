package be.kdg.pro1.m4;

public class FormattingData {
  public static void main(String[] args) {
    System.out.printf("The %dth letter is %c\n", 5, 'e');

    double sum = 164.525;
    String s = String.format("€%.2f %d%% VAT incl.", sum, 21);
    System.out.println(s);

    System.out.println();
    System.out.println("printing doubles:");
    System.out.printf("%b %c %d %f %s%n"
      , true, 'X', 1, 3.14, "abc");

    double value = 1.35798642;
    System.out.printf("%10.8f%n", value);
    System.out.printf("%10.6f%n", value);
    System.out.printf("%10.4f%n", value);
    System.out.printf("%10.2f%n", value);
    System.out.printf("%.1f%n", value);

    System.out.println();
    System.out.println("aligning integers:");
    System.out.format("%4d%n", 1);
    System.out.format("%4d%n", 12);
    System.out.format("%4d%n", 123);
    System.out.format("%4d%n", 1234);
    System.out.format("%4d%n", 12345);
    System.out.format("%4d%n", 123456);

    System.out.println();
    System.out.println("aligning strings:");
    System.out.format("%-10s%n", "Paris");
    System.out.format("%-10s%n", "Madrid");
    System.out.format("%-10s%n", "London");
    System.out.format("%-10s%n", "Berlin");


    String city = "Antwerp";
    System.out.printf("%-11s!%n", city);

  }
}
