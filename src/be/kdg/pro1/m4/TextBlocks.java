package be.kdg.pro1.m4;

public class TextBlocks {
	public static void main(String[] args) {
		String sql = """
			SELECT id, firstName, lastName
			FROM Employee
			WHERE departmentId = "IT"
			ORDER BY lastName, firstName""";
		System.out.println(sql);
	}
}
