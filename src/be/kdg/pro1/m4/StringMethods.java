package be.kdg.pro1.m4;

public class StringMethods {
  public static void main(String[] args) {
    String male = "John";
    String female = "Mary";

    System.out.println(male + ".length() = " + male.length());
    System.out.println(female + ".charAt(3) = " + female.charAt(3));
    System.out.println(male + ".equals(" + female + ") = " + male.equals(female));
    System.out.println(male + ".equals(\"John\") = " + male.equals("John"));
    System.out.println(male + ".equalsIgnoreCase(\"john\") = " + male.equalsIgnoreCase("john"));
    System.out.println();
    System.out.println(male + ".compareTo(" + female + ") = " + male.compareTo(female));
    System.out.println(male + ".compareTo(\"John\") = " + male.compareTo("John"));
    System.out.println(female + ".compareTo(" + male + ") = " + female.compareTo(male));
    System.out.println(male + ".isEmpty() = " + male.isEmpty());

  }
}
